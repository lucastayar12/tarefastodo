package com.example.tarefastodo

import android.content.Context
import android.view.View
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat

class ListViews {

     fun populaView(t: Tarefa, context : Context): View {

        val v = View.inflate(context, R.layout.tarefa_card, null)
        val txtTarefa: TextView = v.findViewById(R.id.txtTarefa)
        val checkBoxSon: CheckBox = v.findViewById(R.id.checkBox0)
        checkBoxSon.isChecked = t.feito
        checkBoxSon.setOnCheckedChangeListener { buttonView, isChecked ->
            t.feito = isChecked
        }
        txtTarefa.text = t.tarefa
        val urgent: LinearLayout = v.findViewById(R.id.Urgent)
        if (t.urgencia) {
            urgent.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
        } else {
            urgent.setBackgroundColor(ContextCompat.getColor(context, R.color.green))
        }
        return v
    }
}