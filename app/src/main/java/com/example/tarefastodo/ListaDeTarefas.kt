package com.example.tarefastodo

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import androidx.core.util.forEach
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListaDeTarefas : AppCompatActivity() {

    private lateinit var fabDeleteTarefa: FloatingActionButton
    private lateinit var adapter: TarefaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lista_tarefas)

        this.fabDeleteTarefa = findViewById(R.id.fabDeleteTarefa)
        val tarefas = TarefaDAOSingleton.getTarefa()
        val rvTarefas = findViewById<View>(R.id.rvTarefas) as RecyclerView
        this.adapter = TarefaAdapter(TarefaDAOSingleton.getTarefa()).setOnChangeSelectionListener{
            if(it.size() != 0) {
                this.fabDeleteTarefa.visibility = View.VISIBLE
            }else{
                this.fabDeleteTarefa.visibility = View.GONE
                }
            }

        rvTarefas.adapter = adapter
        rvTarefas.layoutManager = LinearLayoutManager(this)

        val checkBoxGod: CheckBox = findViewById(R.id.checkBoxGod)

        checkBoxGod.setOnCheckedChangeListener { _, isCheked ->
            rvTarefas.removeAllViewsInLayout()
            for (tarefa in tarefas) {
                if (isCheked) {
                    if (!tarefa.feito) {
                        adapter.notifyItemChanged(TarefaDAOSingleton.updateTarefa(tarefa, tarefa.feito))
                        rvTarefas.adapter = adapter
                    }
                } else {
                    adapter.notifyItemChanged(TarefaDAOSingleton.updateTarefa(tarefa, tarefa.feito))
                    rvTarefas.adapter = adapter
                }
            }
        }
    }

    fun onClickDeleteSelectedTarefas(v: View) {
        adapter.selectedTarefas.forEach { _, value ->
            val pos = TarefaDAOSingleton.deleteTarefa(value)
            adapter.notifyItemRemoved(pos)
            }
        adapter.selectedTarefas.clear()
        fabDeleteTarefa.visibility = View.GONE
    }

}