package com.example.tarefastodo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Switch

class MainActivity : AppCompatActivity() {

    private lateinit var txtTask: EditText
    private lateinit var switchTask: Switch


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.txtTask = findViewById(R.id.txtTask)
        this.switchTask = findViewById(R.id.switchUrgent)

    }

    fun submit(view: View) {
        val tarefa = Tarefa()

        val tarefaMsg = txtTask.text.toString()
        tarefa.tarefa = tarefaMsg

        tarefa.urgencia = switchTask.isChecked == true

        TarefaDAOSingleton.add(tarefa)

        this.txtTask.setText("")
        this.switchTask.isChecked = false

    }

    fun tasks(view: View) {
        val intent = Intent(this, ListaDeTarefas::class.java).apply {
            putExtra("tarefas", TarefaDAOSingleton.getTarefa())
        }
        startActivity(intent)
    }
}