package com.example.tarefastodo

import java.io.Serializable

class Tarefa : Serializable {

    var id : Int = 0
    var tarefa: String = ""
    var urgencia: Boolean = false
    var feito: Boolean = false
        get() = field
        set(value) {
            field = value
        }

}