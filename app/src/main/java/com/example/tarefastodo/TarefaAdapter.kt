package com.example.tarefastodo

import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class TarefaAdapter(private val mTarefas : List<Tarefa>) : RecyclerView.Adapter<TarefaAdapter.ViewHolder>() {

    private var listener: OnClickTarefaListener? = null
    private var selectionListener: OnChangeSelectionListener? = null

    val selectedTarefas: SparseArray<Tarefa> = SparseArray()

    fun interface OnClickTarefaListener {
        fun onRequestTarefa(tarefa: Tarefa)
    }

    fun interface OnChangeSelectionListener {
        fun onChangeSelection(selectedIds: SparseArray<Tarefa>)
    }


    inner class ViewHolder(itemView: View, private val adapter: TarefaAdapter) : RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener{
        val txtTarefa = itemView.findViewById<TextView>(R.id.txtTarefa)
        val urgent = itemView.findViewById<LinearLayout>(R.id.Urgent)
        val context = itemView.context
        val feito = itemView.findViewById<CheckBox>(R.id.checkBox0)
        private lateinit var tarefa: Tarefa


        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }
        fun bind(tarefa: Tarefa) {
            this.tarefa = tarefa
            itemView.isSelected = this.adapter.selectedTarefas.get(this.tarefa.id) != null
        }



        override fun onLongClick(v: View?): Boolean {
            itemView.isSelected = !itemView.isSelected
            if(this.adapter.selectedTarefas.get(this.tarefa.id) != null)
                this.adapter.selectedTarefas.delete(this.tarefa.id)
            else
                this.adapter.selectedTarefas.append(this.tarefa.id, this.tarefa)

            val newSelectedArray = this.adapter.selectedTarefas.clone()
            this.adapter.getOnChangeSelectionListener()?.onChangeSelection(newSelectedArray)

            return true
        }

        override fun onClick(v: View?) {
            this.adapter.getOnClickTarefaListener()?.onRequestTarefa(this.tarefa)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val TarefaView = inflater.inflate(R.layout.tarefa_card, parent, false)

        return ViewHolder(TarefaView, this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tarefa : Tarefa = mTarefas.get(position)
        val textView = holder.txtTarefa
        textView.setText(tarefa.tarefa)
        val urgent = holder.urgent
        if (tarefa.urgencia) {
            urgent.setBackgroundColor(ContextCompat.getColor(holder.context, R.color.red))
        } else {
            urgent.setBackgroundColor(ContextCompat.getColor(holder.context   , R.color.green))
        }

        val feito = holder.feito
        feito.isChecked = tarefa.feito
        feito.setOnCheckedChangeListener { buttonView, isChecked ->
            tarefa.feito = isChecked
        }

        holder.bind(this.mTarefas[position])

    }

    override fun getItemCount(): Int {
        return  mTarefas.size
    }

    fun setOnClickTarefaListener(listener: OnClickTarefaListener?): TarefaAdapter {
        this.listener = listener
        return this
    }
    fun getOnClickTarefaListener(): OnClickTarefaListener? {
        return this.listener
    }

    fun setOnChangeSelectionListener(selectionListener: OnChangeSelectionListener?): TarefaAdapter {
        this.selectionListener = selectionListener
        return this
    }
    fun getOnChangeSelectionListener(): OnChangeSelectionListener? {
        return this.selectionListener
    }


}