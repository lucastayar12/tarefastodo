package com.example.tarefastodo

object TarefaDAOSingleton {
    private val tarefas: ArrayList<Tarefa> = ArrayList()

    fun add(tarefa: Tarefa): Int {
        tarefas.add(tarefa)
        tarefa.id = tarefa.id++
        return  tarefa.id
    }

    fun getTarefa(): ArrayList<Tarefa> {
        return tarefas
    }

    fun deleteTarefa(tarefa: Tarefa): Int{
        tarefas.remove(tarefa)
        return tarefas.indexOf(tarefa)
    }

    fun updateTarefa(tarefa: Tarefa, feitoUpdate : Boolean): Int{
        tarefa.feito = feitoUpdate
        tarefas.set(tarefas.indexOf(tarefa), tarefa)
            return tarefas.indexOf(tarefa)
    }
}